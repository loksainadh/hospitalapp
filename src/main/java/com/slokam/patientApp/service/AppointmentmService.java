package com.slokam.patientApp.service;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.datetime.joda.LocalDateParser;
import org.springframework.format.datetime.joda.LocalDateTimeParser;
import org.springframework.stereotype.Service;

import com.slokam.patientApp.dao.AppointmentInteface;
import com.slokam.patientApp.excelReadw.ExcelRead;
import com.slokam.patientApp.exception.ApplicationException;
import com.slokam.patientApp.pojo.Appointment;
import com.slokam.patientApp.pojo.Patient;

import javafx.util.converter.LocalDateTimeStringConverter;
import sun.util.resources.cldr.es.CalendarData_es_PR;

@Service
public class AppointmentmService implements AppointmentServiceInterface{

	private static Logger LOGGER=LoggerFactory.getLogger(AppointmentmService.class);
	@Autowired
	private AppointmentInteface appointmentInteface;
	List<Object[]> li=null;
	@Autowired
	private ExcelRead excelRead;

	public void appointmetnSave(Appointment p) throws ApplicationException {
		appointmentInteface.save(p);
		
	}
	
	

	@Override
	public 	List<String> getDisease() throws ApplicationException {
		LOGGER.debug("enter into date metthods===========");
		Calendar c=Calendar.getInstance();
		int year=c.get(Calendar.YEAR);
		int month=c.get(Calendar.MONTH);
		int date=c.get(Calendar.DATE);
		LOGGER.debug("enter into date metthods==========="+year);
		LOGGER.debug("enter into date metthods==========="+month);
		LOGGER.debug("enter into date metthods==========="+date);

		MyDateClass m=new MyDateClass();
		LocalDateTime date1=null;
		LocalDateTime date2=null;
		String d="2019 01 04";
		String d1="2019 01 26";
		SimpleDateFormat sd=new SimpleDateFormat("yyyy-MM-dd");
		
		
		
		//List<String>	ld=appointmentInteface.getDisease();
		LOGGER.debug("enter into date metthods===========");

		return null;
	}

	@Override
	public List<Object[]> getVisitType(String name) throws ApplicationException {
		
		try {
		LOGGER.debug("enter into visit  metthods===========");
		 li=appointmentInteface.getVisitType(name);
		 
		 
		}
		catch(Exception e) {
			LOGGER.debug("enter into visit  catch block===========");
			throw new ApplicationException("DB problem",e);
			
		}
		LOGGER.debug("exit  into visit  metthods===========");
		
		return li;
	}

	@Override
	public String getPaidOrFree() throws ApplicationException {
		int id=0;
		LocalDateTime	date1=null;
		for(Object[] obj:li) {
		
		
			
			id=(int)obj[0];
			date1=(LocalDateTime)obj[1];
		}
			LocalDateTime ldt=LocalDateTime.now();
			LocalDateTime date=	ldt.minusDays(7);
			
			
			
			
			while(!date.equals(ldt)) {
				
				if(date1.equals(date)){
					
					LOGGER.debug("free appointmenmt============"+date1);
					break;
				}
				else {
					LOGGER.debug("paid appointmenmt============"+500);
					
				}
				
				
				date=date.plusDays(1);
				
				
				
			}
			
			
			LOGGER.debug("free or paid=========="+date);
	//	}
		
Calendar c=Calendar.getInstance();
int day=c.get(Calendar.DATE);
	
	c.add(day, -7);
	
	LOGGER.debug("free or paid"+day);

	
	

		return null;
		
		
	}
	
	



	@Override
	public List<Appointment> appointmetnSave1() throws ApplicationException {

		List<Appointment> la=excelRead.readExcelForAppoit();
		
		List<Appointment> la1=appointmentInteface.saveAll(la);
		
		
		return la1;
	}



	
	
	
	

}
