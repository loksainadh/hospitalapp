package com.slokam.patientApp.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.slokam.patientApp.exception.ApplicationException;
import com.slokam.patientApp.pojo.Appointment;
import com.slokam.patientApp.pojo.Patient;
import com.slokam.patientApp.pojo.Visite;

@Service
public interface PatientServiceInterface {
	
	public abstract void patientSave(Visite p) throws ApplicationException;
		
	
	public abstract List<String> getDoctors(Date date1,Date date2) throws ApplicationException;

	

}
