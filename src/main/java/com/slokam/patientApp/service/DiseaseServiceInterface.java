package com.slokam.patientApp.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.slokam.patientApp.exception.ApplicationException;
import com.slokam.patientApp.pojo.Disease;

@Service
public interface DiseaseServiceInterface {
	public List<String> getPatients(String name) throws ApplicationException;
	public Integer getPatientCount(String name)throws ApplicationException;

	public List<Integer> getMostRecurrentDisease()throws ApplicationException;
	public List<Disease> getDiseases()throws ApplicationException;
	public void getDiseases(Disease diseas)throws ApplicationException;
	
	public List<Date> getSeason1(String diseaseName)throws ApplicationException;
	
	public  void findByDisease1(Disease disease)throws ApplicationException;
	
	
}
