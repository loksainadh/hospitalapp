package com.slokam.patientApp.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.slokam.patientApp.comparator.PatientComparatorSwitch;
import com.slokam.patientApp.dao.Patient1Repositry;
import com.slokam.patientApp.excelReadw.ExcelRead;
import com.slokam.patientApp.exception.ApplicationException;
import com.slokam.patientApp.pojo.Patient;


@Service
public class PateintServiceP implements PatientInterfaceService {
	
	public static Logger LOGGER=LoggerFactory.getLogger(PateintServiceP.class);
	
@Autowired
	 private Patient1Repositry  patient1Repositry;
@Autowired
private ExcelRead  excelRead;
	 
	public void patientSave(Patient p) throws ApplicationException {
		LOGGER.debug("enter  patientSave");
		patient1Repositry.save(p);
		LOGGER.debug("exit  patientSave");
	}

	@Override
	public List<String> getDoctorsName(String name) throws ApplicationException {
		LOGGER.debug("enter  getDoctorsName");
		List<String> ls=patient1Repositry.getDoctorsName(name);
		
		LOGGER.debug("exit  getDoctorsName");
		return ls;
	}

	
	public List<Patient> getList(String option) throws ApplicationException {
		
		LOGGER.debug("enter  getDoctorsName");
		List<Patient> ls=patient1Repositry.findAll();
		PatientComparatorSwitch.doSwitch(ls, option);
		LOGGER.debug("exit  getDoctorsName");
		
		return ls;
	}

	
	
	public List<Patient> pateintSave1() throws ApplicationException {
		LOGGER.debug("enter  pateintSave");
		
	List<Patient> lp=excelRead.readExcel();
	LOGGER.debug("exit  pateintSave");
	List<Patient> lp1=	patient1Repositry.saveAll(lp);
	/*Map<Integer,Integer> pateintMap=new LinkedHashMap<>();
	int i=1;
	for(Patient p:lp1) {
		pateintMap.put(i, p.getId());
		
		i++;
	}
		*/
		LOGGER.debug("exit  pateintSave");
		return lp1;
	}


}
