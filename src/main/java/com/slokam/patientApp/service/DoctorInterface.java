package com.slokam.patientApp.service;

import java.util.List;

import com.slokam.patientApp.exception.ApplicationException;
import com.slokam.patientApp.pojo.Doctor;
import com.slokam.patientApp.pojo.Patient;

public interface DoctorInterface {

	public void doctorSave(Doctor d)throws ApplicationException;
	
	public abstract List<Patient> getPatients(String name)throws ApplicationException;
	
	public abstract void doctorSave() throws ApplicationException;
	
}
