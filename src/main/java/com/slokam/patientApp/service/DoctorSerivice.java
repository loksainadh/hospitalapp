package com.slokam.patientApp.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.slokam.patientApp.controller.DoctorController;
import com.slokam.patientApp.dao.DoctorRepositry;
import com.slokam.patientApp.excelReadw.ExcelRead;
import com.slokam.patientApp.exception.ApplicationException;
import com.slokam.patientApp.pojo.Doctor;
import com.slokam.patientApp.pojo.Patient;

@Service
public class DoctorSerivice  implements DoctorInterface{
	private static Logger LOGGER=LoggerFactory.getLogger(DoctorSerivice.class);
	@Autowired
	private DoctorRepositry doctorRepositry;
	
	
	public void doctorSave(Doctor d) throws ApplicationException {
		LOGGER.debug("enter into doctorSave ");
		
		doctorRepositry.save(d);
		
		LOGGER.debug("exit from doctorSave ");
	}


	@Override
	public List<Patient> getPatients(String name) throws ApplicationException {
		LOGGER.debug("enter into doctorSave ");
		
		List<Patient> ls=doctorRepositry.getPatients(name);
		LOGGER.debug("exit from doctorSave ");
		return ls;
	}
	
	public void doctorSave() throws ApplicationException {
		LOGGER.debug("enter into doctorSave ");
		List<Doctor> ld=ExcelRead.readExcelForDoctor();
		doctorRepositry.saveAll(ld);
		LOGGER.debug("exit from doctorSave ");
		
	}

}
