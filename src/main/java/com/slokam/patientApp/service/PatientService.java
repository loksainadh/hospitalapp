package com.slokam.patientApp.service;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.slokam.patientApp.dao.AppointmentInteface;
import com.slokam.patientApp.dao.PatientInterface;
import com.slokam.patientApp.dao.PatientRepsitory;
import com.slokam.patientApp.exception.ApplicationException;
import com.slokam.patientApp.pojo.Appointment;
import com.slokam.patientApp.pojo.Patient;
import com.slokam.patientApp.pojo.Visite;
@Service
public class PatientService implements PatientServiceInterface {
	
	private static Logger LOGGER=LoggerFactory.getLogger(PatientService.class);
	
	@Autowired
	private PatientRepsitory patientRepsitory;
 @Autowired
	private PatientInterface patientInterface;

	public void patientSave(Visite p) throws ApplicationException {
		LOGGER.debug("we are in service  method");
		patientInterface.patientSave(p);
		
		LOGGER.debug("exit from service  method");
		
	}

	@Override
	public List<String> getDoctors(Date date1, Date date2) throws ApplicationException {
		LOGGER.debug("getDoctors");
		List<String> ls=patientRepsitory.getDoctors(date1, date2);
		LOGGER.debug("exit from getDoctors");
		return ls;
	}
	
	

}
