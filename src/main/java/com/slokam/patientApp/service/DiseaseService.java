package com.slokam.patientApp.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.slokam.patientApp.dao.DiseasRepositry;
import com.slokam.patientApp.exception.ApplicationException;
import com.slokam.patientApp.pojo.Disease;

@Service
public class DiseaseService implements DiseaseServiceInterface {
	
	private static Logger  LOGGER=LoggerFactory.getLogger(DiseaseService.class);
	
	@Autowired
	private DiseasRepositry diseasRepositry;

	public List<String> getPatients(String name) throws ApplicationException {
		LOGGER.debug("disease service");
		List<String> ls=diseasRepositry.getPatients(name);
		LOGGER.debug("disease service");
		return ls;
	}

	
	public Integer getPatientCount(String name) throws ApplicationException {
		LOGGER.debug("count disease service");
		Integer count=diseasRepositry.getPatientCount(name);
		
		LOGGER.debug("count  disease service");
		return count;
	}

	
	public List<Integer> getMostRecurrentDisease() throws ApplicationException {
		Integer count=null;
		LOGGER.debug("count disease service");
		List<Disease> ls=getDiseases();
		List<Integer> li=new ArrayList();
		
		for(int i=0;i<ls.size();i++) {
		 count=diseasRepositry.getMostRecurrentDisease(ls.get(i).getDisease());
		 li.add(count);
		}
		return li;
		
		
	}

	
	public List<Disease> getDiseases() {
		LOGGER.debug("list disease service");
		List<Disease> ld=	diseasRepositry.getDiseases();
		LOGGER.debug("exit list disease service");
		return ld;
	}


	


	@Override
	public List<Date> getSeason1(String diseaseName) throws ApplicationException {
		List<Date> ld=	diseasRepositry.getSeason1(diseaseName);
		return ld;
	}


	@Override
	public void findByDisease1(Disease disease) throws ApplicationException {
		Disease d=diseasRepositry.findByDisease(disease.getDisease());
		try {
		if(d!=null) {
			
			LOGGER.debug("disease service already exits");
			throw new ApplicationException("already exists");
			
		}
		else {
			diseasRepositry.save(disease);
			LOGGER.debug("disease service saved");
		}
		
		}catch(ApplicationException a) {
			throw a;
		}
		catch(Exception a) {
			throw new ApplicationException("db problem");
		}
		
		
		
		
		
	}


	@Override
	public void getDiseases(Disease diseas) throws ApplicationException {
		// TODO Auto-generated method stub
		
	}



}
