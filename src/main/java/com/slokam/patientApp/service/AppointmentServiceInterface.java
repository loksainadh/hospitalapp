package com.slokam.patientApp.service;

import java.util.Date;
import java.util.List;

import com.slokam.patientApp.exception.ApplicationException;
import com.slokam.patientApp.pojo.Appointment;

public interface AppointmentServiceInterface {
	public abstract void appointmetnSave(Appointment p) throws ApplicationException;
	public 	List<String> getDisease()throws ApplicationException;
	public List<Object[]> getVisitType(String name)throws ApplicationException;
	
	public String getPaidOrFree()throws ApplicationException;
	public abstract List<Appointment> appointmetnSave1() throws ApplicationException;
}
