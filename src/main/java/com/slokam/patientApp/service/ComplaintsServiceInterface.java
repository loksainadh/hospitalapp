package com.slokam.patientApp.service;

import org.springframework.stereotype.Service;

import com.slokam.patientApp.exception.ApplicationException;
import com.slokam.patientApp.pojo.Complaints;
@Service
public interface ComplaintsServiceInterface {
	
	public abstract void saveDiseas(Complaints complaints)throws ApplicationException;
}
