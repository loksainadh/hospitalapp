package com.slokam.patientApp.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.slokam.patientApp.exception.ApplicationException;
import com.slokam.patientApp.pojo.Patient;
@Repository
public interface PatientInterfaceService {

	
	public void patientSave(Patient p)throws ApplicationException;
	public abstract List<String> getDoctorsName(String name) throws ApplicationException;
	
	public abstract List<Patient> getList(String option)throws ApplicationException;
	
	public List<Patient>  pateintSave1()throws ApplicationException;
}
