package com.slokam.patientApp.service.mapping;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.slokam.patientApp.pojo.Appointment;
import com.slokam.patientApp.pojo.Patient;

public class AppoimtnetExcelRead {
	
	
	
	public static Map<Integer, Integer> getAppointMap(List<Appointment> appointList){
		
		Map<Integer, Integer> appointmentMap=new HashMap<>();
		int excelId=1;
		for (Appointment appointment : appointList) {
			appointmentMap.put(excelId, appointment.getId());
			excelId++;
		}
		
		
		
		return appointmentMap;
	}
	
	public static List<Appointment> appoitExcelRead(String filepath,String sheetName){
		
		List<Appointment> appList=new ArrayList<>();
		try {
			FileInputStream fir=new FileInputStream(filepath);
			HSSFWorkbook hw=new HSSFWorkbook(fir);
			HSSFSheet hs=hw.getSheet(sheetName);
			int lastRow=hs.getLastRowNum();
			
			for(int i=0;i<=lastRow;i++) {
			HSSFRow hr=	hs.getRow(i);
			Appointment app=new Appointment();
		String reDate=hr.getCell(1).getStringCellValue();
		String appDate=hr.getCell(2).getStringCellValue();
		String time=hr.getCell(3).getStringCellValue();
	   int id=(int)hr.getCell(4).getNumericCellValue();
	
	 Map<Integer,Integer> appointMap=DataImport.patientMap;
	   
	  int originalId= appointMap.get(id);
	  
	   Patient p=new Patient();
	   p.setId(originalId);
	   app.setTime(time);
	   SimpleDateFormat s=new SimpleDateFormat("yyyy-MM-dd");
	 try {
	   Date date1=  s.parse(reDate);
	   Date date2=s.parse(appDate);
	   app.setDateofApp(date2);
	   app.setReqdate(date1);
	 //  app.setPatient(p);
	   appList.add(app);
	 }catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
				
			}
			
			
			
			

		}catch(IOException e) {
			e.printStackTrace();
			
		} 
		
		return appList;
		
	}

}
