package com.slokam.patientApp.service.mapping;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.slokam.patientApp.pojo.Doctor;

public class DoctorExcelRead {
	
	public static Map<Integer, Integer> getDoctorMap(List<Doctor> ld){
		
		Map<Integer, Integer> doctorMap=new HashMap<>();
		int x=1;
		for (Doctor doctor : ld) {
			doctorMap.put(x, doctor.getId());
			x++;
		}
		return doctorMap;
	}
	
	
	public static List<Doctor> getDoctorList(String filepath,String sheetName){
		
		List<Doctor> doctorsList=new ArrayList<>();
		
		try {
		FileInputStream fir=new FileInputStream(filepath);
		HSSFWorkbook hw=new HSSFWorkbook(fir);
	   HSSFSheet hs=hw.getSheet(sheetName);
	   
	  int num= hs.getLastRowNum();
		
	  for(int i=0;i<=num;i++) {
		 HSSFRow hr= hs.getRow(i);
		String name= hr.getCell(1).getStringCellValue();
		String Spec=hr.getCell(2).getStringCellValue();
		
		Doctor d=new Doctor();
		d.setName(name);
		d.setTypeOfDoctor(Spec);
		doctorsList.add(d);
		  
	  }
		
		
		
	}
		catch(IOException e) {
			e.printStackTrace();
			
		
	}
	return doctorsList;

}
	}
