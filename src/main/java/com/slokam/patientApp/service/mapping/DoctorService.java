package com.slokam.patientApp.service.mapping;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.slokam.patientApp.dao.DoctorRepositry;
import com.slokam.patientApp.exception.ApplicationException;
import com.slokam.patientApp.pojo.Appointment;
import com.slokam.patientApp.pojo.Doctor;
import com.slokam.patientApp.pojo.Patient;
@Service
public class DoctorService implements Doctorsinterface{

	@Autowired
	private DoctorRepositry doctorRepositry;
	
	public static Map<Class,Map<Integer, Integer>> map=null;
	
	@Override
	public void importData(String filepath) throws ApplicationException {
		
		map=new HashMap<>();
		Map<Class,Map<Integer, Integer>> map2=AppointmentService.map;
		Map<Integer, Integer> appoit=map2.get(Appointment.class);
		
		
		List<Doctor> doctoslist=DoctorExcelRead.getDoctorList(filepath, "Doctor.java");
		
		
		List<Doctor> latestdoctoslist=doctorRepositry.saveAll(doctoslist);
		 Map<Integer, Integer> doctorsMap=DoctorExcelRead.getDoctorMap(latestdoctoslist);
		map.put(Doctor.class, doctorsMap);
		}

}
