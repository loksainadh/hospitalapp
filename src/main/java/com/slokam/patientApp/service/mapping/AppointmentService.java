package com.slokam.patientApp.service.mapping;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.slokam.patientApp.dao.AppointmentInteface;
import com.slokam.patientApp.exception.ApplicationException;
import com.slokam.patientApp.pojo.Appointment;
import com.slokam.patientApp.pojo.Patient;
@Service
public class AppointmentService implements AppintmetInterface {
	
	IDataImportInterface iDataImportInterface;

	@Autowired
	private AppointmentInteface appointmentInteface;
	 public static Map<Class,Map<Integer, Integer>> map=null;
	 
	
	@Override
	public void appoitmnetMap(String filepath) throws ApplicationException {
		map=new HashMap<>();
		Map<Integer, Integer> patient=	DataImport.patientMap;
		Map<Class,Map<Integer, Integer>> map1=DataImport.map;
		
		
		Map<Integer, Integer> patinmap=map1.get(Patient.class);
		
		String sheetName="Appointment.java";
		List<Appointment> lp=AppoimtnetExcelRead.appoitExcelRead(filepath, sheetName);

		List<Appointment> latestAppoitment=appointmentInteface.saveAll(lp);
		Map<Integer, Integer> appint= AppoimtnetExcelRead.getAppointMap(latestAppoitment);
		
		map.put(Appointment.class, appint);
	}

}
