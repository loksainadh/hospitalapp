package com.slokam.patientApp.service.mapping;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.slokam.patientApp.exception.ApplicationException;
import com.slokam.patientApp.pojo.Patient;

public class PateintExcelRead {

	private static Logger LOGGER=LoggerFactory.getLogger(PateintExcelRead.class);
	
	
	public static Map<Integer, Integer> getPatientMap(List<Patient> latest){
		
		 Map<Integer, Integer> pateintMap=new HashMap<>();
		 int exId=1;
		 for (Patient patient : latest) {
			 pateintMap.put(exId,patient.getId());
			 
			 exId++;
			
		}
		 
		 return pateintMap;
		 
		
	}
	
	
	public static  List<Patient> readExcel(String filepath,String sheetName)throws ApplicationException{
	
		LOGGER.debug("enter excel read   =================="+filepath);
		List<Patient> lp=new ArrayList<>();
		
		try {
		FileInputStream fir=new FileInputStream(filepath);
		LOGGER.debug("try block   ==================");
		
		HSSFWorkbook hw=new HSSFWorkbook(fir);
		HSSFSheet hs=hw.getSheetAt(0);
		HSSFSheet hs1=hw.getSheet(sheetName);
		
		int num=hs.getLastRowNum();
		
		for(int i=0;i<num;i++) {
			LOGGER.debug("enter excel read   ==================loop");
			HSSFRow hr=hs.getRow(i);
			Patient p=new Patient();
			p.setName(hr.getCell(1).getStringCellValue());
			int age=(int)hr.getCell(2).getNumericCellValue();
			p.setAge(age);
			
			LOGGER.debug("enter excel read   =================="+p.getName());
			p.setWeight((int)hr.getCell(3).getNumericCellValue());
			String date=hr.getCell(4).getStringCellValue();
			
			try {
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
			
			Date date1=sdf.parse(date);
			
			p.setDate(date1);
			
			}
			catch(Exception e) {
				e.printStackTrace();
				
			}
			
			LOGGER.debug("excell read========="+p.getDate());
			
			lp.add(p);
			
			
			
			
			
		}
		
		
		
		
		fir.close();
		}
		catch(IOException e) {
			e.printStackTrace();
			throw new ApplicationException("Db problem",e);
			
		}
		
		return lp;
		
	}
	
	

}
