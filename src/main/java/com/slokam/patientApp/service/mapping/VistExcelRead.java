package com.slokam.patientApp.service.mapping;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.slokam.patientApp.pojo.Appointment;
import com.slokam.patientApp.pojo.Doctor;
import com.slokam.patientApp.pojo.Visite;

public class VistExcelRead {
	
	public static  List<Visite>  getVisitList(String filepath,String sheetName){
		
		List<Visite> visitList=new ArrayList<>();
		try {
			FileInputStream fir=new FileInputStream(filepath);
			HSSFWorkbook hw=new HSSFWorkbook(fir);
			HSSFSheet hs=hw.getSheet(sheetName);
			int lastRow=hs.getLastRowNum();
			
			for(int i=0;i<=lastRow;i++) {
			HSSFRow hr=	hs.getRow(i);
			Visite v=new Visite();
			
			Map<Integer,Integer> appoitMap=AppointmentService.appointmentMap;
			Map<Integer,Integer> doctorMap=DoctorService.doctorsMap;
			int appid=(int)hr.getCell(1).getNumericCellValue();
			int docid=(int)hr.getCell(2).getNumericCellValue();
			
			int dbidApp=appoitMap.get(appid);
			int dbIdDoc=doctorMap.get(docid);
			
			Appointment a=new Appointment();
			a.setId(dbidApp);
			
			Doctor dd=new Doctor();
			dd.setId(dbIdDoc);
			
			
			String date=hr.getCell(3).getStringCellValue();
			SimpleDateFormat s=new SimpleDateFormat("yyyy-MM-dd");
			Date date1=s.parse(date);
			v.setVisitDate(date1);
			v.setDoctor(dd);
			v.setAppointment(a);
			visitList.add(v);
		
	}

}catch(IOException e) {
	e.printStackTrace();
	
} catch (ParseException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
return 	visitList;	
}}