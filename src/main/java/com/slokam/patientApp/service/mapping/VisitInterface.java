package com.slokam.patientApp.service.mapping;

import com.slokam.patientApp.exception.ApplicationException;

public interface VisitInterface {
	
	void importVisitData(String filepath)throws ApplicationException;

}
