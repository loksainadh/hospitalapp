package com.slokam.patientApp.service.mapping;

import com.slokam.patientApp.exception.ApplicationException;

public interface Doctorsinterface {
	void importData(String filepath)throws ApplicationException;

}
