package com.slokam.patientApp.service.mapping;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.slokam.patientApp.dao.PatientRepsitory;
import com.slokam.patientApp.exception.ApplicationException;
import com.slokam.patientApp.pojo.Appointment;
import com.slokam.patientApp.pojo.Visite;

@Service
public class VisitService implements VisitInterface{

	@Autowired
	private PatientRepsitory patientRepsitory;
	
	private static Map<Class,Map<Integer, Integer>> map=null;
	@Override
	public void importVisitData(String filepath) throws ApplicationException {
		
		map=new HashMap<>();
		Map<Class,Map<Integer, Integer>> map2=AppointmentService.map;
		Map<Integer, Integer> appoit=map2.get(Appointment.class);
		
		
	List<Visite> vlist=	VistExcelRead.getVisitList(filepath, "visite.java");
	patientRepsitory.saveAll(vlist);
	}

}
