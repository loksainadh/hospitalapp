package com.slokam.patientApp.service.mapping;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.record.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.slokam.patientApp.dao.Patient1Repositry;
import com.slokam.patientApp.exception.ApplicationException;
import com.slokam.patientApp.pojo.Patient;

@Service
public class DataImport implements IDataImportInterface {
	@Autowired
	private Patient1Repositry patient1Repositry;
	
	
	public static Map<Class,Map<Integer, Integer>> map=null;
	public static  Map<Integer, Integer> patientMap=null;
	@Override
	public void importDataAndSave(String filepath) throws ApplicationException {
		
		map=new HashMap<>();
		
		Map<Integer, Integer> patientMap=new HashMap<>();
		
		List<Patient> patintList=PateintExcelRead.readExcel(filepath,"Patient.java");

		List<Patient> latestPatintList=patient1Repositry.saveAll(patintList);
		
		 patientMap=PateintExcelRead.getPatientMap(latestPatintList);
		
		map.put(Patient.class, patientMap);
	}

}
