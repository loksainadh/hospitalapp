package com.slokam.patientApp.service.mapping;

import org.springframework.stereotype.Service;

import com.slokam.patientApp.exception.ApplicationException;


public interface IDataImportInterface {
	
	public void importDataAndSave(String filepath)throws ApplicationException;

}
