package com.slokam.patientApp.comparator;

import java.util.Comparator;

import com.slokam.patientApp.pojo.Patient;

public class PatientComparatorForName implements Comparator<Patient> {

	@Override
	public int compare(Patient o1, Patient o2) {
	
		return o1.getName().compareTo(o2.getName());
	}

}
