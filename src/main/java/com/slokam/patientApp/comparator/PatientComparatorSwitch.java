package com.slokam.patientApp.comparator;

import java.util.Collections;
import java.util.List;

import com.slokam.patientApp.pojo.Patient;

public abstract class PatientComparatorSwitch {
	
	public static  List<Patient> doSwitch(List<Patient> ls,String option){
		
		switch(option) {
		
		case "id":Collections.sort(ls,new PatientComparatorForId());
		    break;
		case "name":Collections.sort(ls,new PatientComparatorForName());
		         break;
		case "age":Collections.sort(ls,new PatientcomparatorAge());
        break;
		case "weight":Collections.sort(ls,new PatientcomparatorWeight());
        break;
		
		
		}
		
		return ls;
	}

}
