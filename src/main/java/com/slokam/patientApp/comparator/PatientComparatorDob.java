package com.slokam.patientApp.comparator;

import java.util.Comparator;

import com.slokam.patientApp.pojo.Patient;

public class PatientComparatorDob implements Comparator<Patient> {

	@Override
	public int compare(Patient o1, Patient o2) {
		// TODO Auto-generated method stub
		return o1.getDate().compareTo(o2.getDate());
	}

}
