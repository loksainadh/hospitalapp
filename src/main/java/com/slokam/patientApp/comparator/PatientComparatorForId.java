package com.slokam.patientApp.comparator;

import java.util.Comparator;

import com.slokam.patientApp.pojo.Patient;

public class PatientComparatorForId  implements Comparator<Patient>{

	
	public int compare(Patient o1, Patient o2) {
		
		return o1.getId().compareTo(o2.getId());
	}

}
