package com.slokam.patientApp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionHandler1 {
	
	@ExceptionHandler(ApplicationException.class)
	public ResponseEntity<String> excptionhandler(ApplicationException a){
		
		ResponseEntity<String> re=new ResponseEntity<>(a.getMessage()+""+a.getCause(),HttpStatus.INTERNAL_SERVER_ERROR);
		return re;
	}

}
