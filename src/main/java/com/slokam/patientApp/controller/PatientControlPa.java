package com.slokam.patientApp.controller;

import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.slokam.patientApp.excelReadw.ExcelRead;
import com.slokam.patientApp.exception.ApplicationException;
import com.slokam.patientApp.pojo.Patient;

import com.slokam.patientApp.service.PatientInterfaceService;

@RestController
@RequestMapping("patient")
public class PatientControlPa {

	private static Logger LOGGER=LoggerFactory.getLogger(PatientControlPa.class);
	
	
	
	@Autowired
	private ExcelRead excelRead;
	@Autowired
	private PatientInterfaceService patientInterfaceService;
	@PostMapping
	public ResponseEntity<String> pateintSave(@RequestBody Patient p)throws ApplicationException {
		
		ResponseEntity<String> responseEntity=null;
		LOGGER.debug("we are in contrller");
		
		try {
			if(p!=null) {
			LOGGER.debug("patent ====="+p);
			patientInterfaceService.patientSave(p);
			responseEntity=new ResponseEntity<>("Successfully inserted",HttpStatus.OK);
			
			}
			else {
				responseEntity=new ResponseEntity<>(HttpStatus.BAD_REQUEST);
				
			}
			
			
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		LOGGER.debug("exit from contrller");
		
		return responseEntity;
		
		
	}
	@GetMapping
	public ResponseEntity<List<String>> getDoctorsName(String name) throws ApplicationException {
		
		LOGGER.debug("we are in  getDoctorsName contrller");

		ResponseEntity<List<String>> res=null;
		try {
		if(name!=null) {
		
		List<String> ls=patientInterfaceService.getDoctorsName(name);
		
		 res=new ResponseEntity<List<String>>(ls,HttpStatus.OK);
		}
		else {
			LOGGER.debug("problem while sending");
			res=new ResponseEntity<List<String>>(HttpStatus.BAD_REQUEST);

		}}
		catch(Exception e) {
			e.printStackTrace();
			
			
		}
		
		LOGGER.debug("exit from getDoctorsName contrller");
		return res;
	
	}
	
@GetMapping("/getlistp/{option}")
public List<Patient> getList(@PathVariable String option) throws ApplicationException {
	LOGGER.debug("enter into get list  metthods===========");
	List<Patient> lp=patientInterfaceService.getList(option);
	
	LOGGER.debug("exit from get list  metthods===========");
	
	return lp;
}


@GetMapping("/map")
public void pateintSave1() throws ApplicationException {
	LOGGER.debug("enter  pateintSave");
	

LOGGER.debug("exit  pateintSave");
List<Patient> lp1=	patientInterfaceService.pateintSave1();
//return lp1;
/*@PostMapping("/all1")
public ResponseEntity<String> pateintSave() throws ApplicationException {
	LOGGER.debug("enter  pateintSave");
	
	patientInterfaceService.pateintSave();
	ResponseEntity<String> re =new ResponseEntity<String>("succefully inserted",HttpStatus.OK);
	LOGGER.debug("exit  pateintSave");
	return re;
}*/

}
	
	
}
