package com.slokam.patientApp.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.slokam.patientApp.exception.ApplicationException;
import com.slokam.patientApp.pojo.Doctor;
import com.slokam.patientApp.pojo.Patient;
import com.slokam.patientApp.service.DoctorInterface;

@RestController
@RequestMapping("doctor")
public class DoctorController {
	
	private static Logger LOGGER=LoggerFactory.getLogger(DoctorController.class);
   @Autowired
	private DoctorInterface doctorInterface;
   @PostMapping
   public ResponseEntity<String> doctorSave(@RequestBody Doctor doctor)throws ApplicationException{
	   LOGGER.debug("enter into doctorSave");
	   ResponseEntity<String> re=null;
	   try {
		if(doctor!=null) {
			   LOGGER.debug("doctor "+doctor);
			   
			   doctorInterface.doctorSave(doctor);
			  re=new  ResponseEntity<>(HttpStatus.OK);
		   }else {
			   re=new  ResponseEntity<>(HttpStatus.BAD_REQUEST);
		   }
	} catch (Exception e) {
		
		e.printStackTrace();
	}
	   return re;
   }
   
   @GetMapping("/{name}")
   public ResponseEntity<List<Patient>> getPatients(@PathVariable String name)throws ApplicationException{
	   LOGGER.debug("we are into get patients data");
	   ResponseEntity<List<Patient>> res=null;
	   try {
		   if(name!=null) {
			   List<Patient> ls=   doctorInterface.getPatients(name);
	   
	    res=new ResponseEntity<List<Patient>>(ls,HttpStatus.OK);
	    LOGGER.debug("exit from get patients data");
	   }
	   else {
		   res=new ResponseEntity<List<Patient>>(HttpStatus.BAD_REQUEST);
	   }}
		   catch(Exception e) {
			   e.printStackTrace();
		   }
	   
	   
	   return res;
	   
   }
   
}
