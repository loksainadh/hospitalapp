package com.slokam.patientApp.controller;

import java.util.Date;
import java.util.List;

import javax.persistence.GeneratedValue;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.slokam.patientApp.exception.ApplicationException;
import com.slokam.patientApp.pojo.Disease;
import com.slokam.patientApp.service.DiseaseService;
import com.slokam.patientApp.service.DiseaseServiceInterface;
@RestController
@RequestMapping("disease")
public class DiseasControler {
	
	private static Logger  LOGGER=LoggerFactory.getLogger(DiseasControler.class);
	
	@Autowired
	private DiseaseServiceInterface diseaseServiceInterface;
	
	
	
	@PostMapping
	public ResponseEntity<String> getDiseasesSave(@Valid @RequestBody Disease diseas) throws ApplicationException{
		LOGGER.debug("list disease service");
		diseaseServiceInterface.getDiseases(diseas);
		ResponseEntity<String> re =new ResponseEntity<String>(HttpStatus.OK);
		LOGGER.debug("exit list disease service");
		return re;
	}
	
	@GetMapping("/{name}")
	public ResponseEntity<List<String>> getPatients(@PathVariable String name) throws ApplicationException {
		LOGGER.debug("controller disease");
		
		ResponseEntity<List<String>> ls=null;
		
		if(name!=null) {
		List<String> ls1=diseaseServiceInterface.getPatients(name);
		
		ls=new ResponseEntity<List<String>>(ls1,HttpStatus.OK);
		
		}
		else {
			LOGGER.debug("null values in controller disease");
			ls=new ResponseEntity<List<String>>(HttpStatus.BAD_REQUEST);	
		}
		
		LOGGER.debug(" exit controller disease");
		return ls;
	}
	@GetMapping("/count")
	public ResponseEntity<Integer> getPatientCount(@RequestParam String name) throws ApplicationException {
		LOGGER.debug("count disease service");
		ResponseEntity<Integer> re=null;
		
		if(name!=null) {
		Integer count=diseaseServiceInterface.getPatientCount(name);
		
		
		re=new ResponseEntity<Integer>(count,HttpStatus.OK);
		}
		else {
			re=new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);
			
		}
		
		LOGGER.debug("count  disease service");
		return re;
	}
	
	


	
	@GetMapping("/recur")
	public List<Integer> getMostRecurrentDisease() throws ApplicationException {
	//	Integer count=null;
		LOGGER.debug("count disease service");
		
		List<Integer> count=diseaseServiceInterface.getMostRecurrentDisease();
		
		return count;
		
		
	}
	@GetMapping("/list")
	public ResponseEntity<List<Disease>> getDiseases() {
		LOGGER.debug("list disease service");
		//List<Disease> ld=	diseaseServiceInterface.getDiseases();
	//	ResponseEntity<List<Disease>> re =new ResponseEntity<List<Disease>>(ld,HttpStatus.OK);
		LOGGER.debug("exit list disease service");
		return null;
	}
	
	@GetMapping("/season")
	public ResponseEntity<List<Date>> getSeason1(String diseaseName) throws ApplicationException {
		
		ResponseEntity<List<Date>> re=null;
		if(diseaseName!=null) {
			LOGGER.debug("Date disease service");
		List<Date> ld=	diseaseServiceInterface.getSeason1(diseaseName);
		
		re=new ResponseEntity<List<Date>>(ld,HttpStatus.OK);
		LOGGER.debug(" exit Date disease service");
		
		}
		return re;
	}
	
	@PostMapping("/dis")
	public ResponseEntity<String> findByDisease(@Valid @RequestBody Disease disease) throws ApplicationException {
		LOGGER.debug("enter disease controller ");
		diseaseServiceInterface.findByDisease1(disease);
		
		ResponseEntity<String> re=new ResponseEntity<String>(HttpStatus.CREATED);
		LOGGER.debug("exit disease controller");
		return re;
		
	}




}
