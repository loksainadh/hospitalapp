package com.slokam.patientApp.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.slokam.patientApp.exception.ApplicationException;
import com.slokam.patientApp.pojo.Appointment;
import com.slokam.patientApp.pojo.Patient;
import com.slokam.patientApp.service.AppointmentServiceInterface;

@RestController
@RequestMapping("appointmentControl")
public class AppointmentController {
	
	private static Logger LOGGER=LoggerFactory.getLogger(AppointmentController.class);
	@Autowired
	private AppointmentServiceInterface appointmentServiceInterface;
	private List<Object[]> li=null;
	
	@PostMapping
	public ResponseEntity<String> appointmentSave(@RequestBody Appointment a)throws ApplicationException{
		LOGGER.debug("enter into appointmentSave ");
		 ResponseEntity<String> re=null;
		 a.setDateofApp(new Date());
		try {
			if(a!=null) {
				LOGGER.debug("appointment === "+a);
				
				appointmentServiceInterface.appointmetnSave(a);
				
			 re=new  ResponseEntity<>(HttpStatus.OK);
			 
			}
		} catch (Exception e) {
			
			
			 re=new  ResponseEntity<>(HttpStatus.BAD_REQUEST);
			
		}
		 
		LOGGER.debug("exit from appointmentSave ");
		 return re;
	}
	@GetMapping
	public ResponseEntity<List<String>> getDisease() throws ApplicationException {
		List<String>	ls=appointmentServiceInterface.getDisease();
		ResponseEntity<List<String>>  re=new ResponseEntity<>(ls,HttpStatus.OK);
		return re;
		
		
	}
	@GetMapping("/vi")
public List<Object[]> getVisitType(String name) throws ApplicationException {
		
		LOGGER.debug("enter into visit  metthods===========");
		
		 li=appointmentServiceInterface.getVisitType(name);
		
		LOGGER.debug("exit  into visit  metthods===========");
		
		return li;
	}
@GetMapping("/type")
public String getPaidOrFree() throws ApplicationException {

/*for(Object obj:li) {
	LOGGER.debug("free or paid"+obj);
	
}*/
	LOGGER.debug("enter into paid or free  metthods===========");
	appointmentServiceInterface.getPaidOrFree();
	LOGGER.debug("exit into paid or free  metthods===========");

		return null;
		

}
@GetMapping("/map")
public List<Appointment> appointmetnSave1() throws ApplicationException {

	LOGGER.debug("map method===========");
	
	List<Appointment> la1=appointmentServiceInterface.appointmetnSave1();
	
	LOGGER.debug("exit map method===========");
	
	return la1;
}
}
