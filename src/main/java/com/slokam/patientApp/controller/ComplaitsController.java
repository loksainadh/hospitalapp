package com.slokam.patientApp.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.slokam.patientApp.exception.ApplicationException;
import com.slokam.patientApp.pojo.Complaints;
import com.slokam.patientApp.service.ComplaintsServiceInterface;

@RestController
@RequestMapping("complaints")
public class ComplaitsController {
	
	
	
	private static Logger LOGGER=LoggerFactory.getLogger(ComplaitsController.class);
	
	@Autowired
	private ComplaintsServiceInterface complaintsServiceInterface;
	
	@PostMapping
	public ResponseEntity<String> diseasSave(@RequestBody Complaints complaits) throws ApplicationException {
		LOGGER.debug(" enter complaints controller");
		ResponseEntity<String> res=null;
		
		
		if(complaits!=null) {
			
		complaintsServiceInterface.saveDiseas(complaits);
		res =new ResponseEntity<String>(HttpStatus.OK);
		}
		else {
			res=new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
			LOGGER.debug(" complaints controller null values");
		}
		LOGGER.debug("exit from complaints controller");
		return res;}

}
