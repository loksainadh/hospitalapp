package com.slokam.patientApp.controller;

import java.io.File;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.slokam.patientApp.excelReadw.ExcelRead;
import com.slokam.patientApp.exception.ApplicationException;
import com.slokam.patientApp.service.mapping.AppintmetInterface;
import com.slokam.patientApp.service.mapping.Doctorsinterface;
import com.slokam.patientApp.service.mapping.IDataImportInterface;
import com.slokam.patientApp.service.mapping.VisitInterface;


@RestController
@RequestMapping("mul")
public class MultifileController {
	@Autowired
	private AppintmetInterface appintmetInterface;
	@Autowired
	private Doctorsinterface doctorsinterface;
	@Autowired
	private VisitInterface visitInterface;
	
	@Autowired
	private IDataImportInterface iDataImportInterface;
	Logger LOGGER=LoggerFactory.getLogger(MultifileController.class);
	
	@PostMapping
	public void multiPartSave(MultipartFile file) throws ApplicationException{
		LOGGER.debug("file name======"+file.getOriginalFilename());
		LOGGER.debug("file lenght======"+file.getSize());
		
		String filepath="F:\\A\\multipart\\"+file.getOriginalFilename();
		
		
		try {
		File f=new File(filepath);
		file.transferTo(f);
		
		ExcelRead.filepath=filepath;
		
		
		
		}
		catch(Exception e) {
			e.printStackTrace();
			
		}
		
		
		iDataImportInterface.importDataAndSave(filepath);
		appintmetInterface.appoitmnetMap(filepath);
		doctorsinterface.importData(filepath);
		visitInterface.importVisitData(filepath);
		
		
	}

}
