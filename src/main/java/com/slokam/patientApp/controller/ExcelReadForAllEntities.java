package com.slokam.patientApp.controller;

import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.slokam.patientApp.pojo.Appointment;

public class ExcelReadForAllEntities {

	public static void main(String[] args) {
		//List<Appointment> obj=getObjectList("F:\\A\\pat.xls", "Appointment.java", Appointment.class,map);
		
		/*Map<Integer,Integer> k=getMap(obj,Appointment.class);
		int ff=1;
		for (Object object : obj) {
			System.out.println(object);
		System.out.println(	k.get(ff));
			ff++;
		}*/
		
		
		
		
	}
	
	public static <T> Map<Integer,Integer> getMap(List<T> objecttype,Class cls) {
		
		Map<Integer,Integer> typeMap=new HashMap<>();
		
		
		
		try {
			Field field=cls.getDeclaredField("id");
			
			field.setAccessible(true);
			int i=2;
			for(T t:objecttype) {
				
				typeMap.put(i, (Integer)field.get(t));
			}
			
		} catch (NoSuchFieldException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		return typeMap;
	}
	

	public static <T> List<T> getObjectList(String filepath, String sheetName, Class cls,Map<Class, Map<Integer, Integer>> map) {
		List<T>  obj1=new ArrayList<>();
		try {
			
			FileInputStream fis = new FileInputStream(filepath);
			HSSFWorkbook hw = new HSSFWorkbook(fis);
			HSSFSheet hs = hw.getSheet(sheetName);
			int rowNum = hs.getLastRowNum();
			HSSFRow hr = hs.getRow(0);
			int lastcolum = hr.getLastCellNum();
			
			
			Field field = null;
			String[] array = new String[lastcolum];
			for (int i = 0; i < lastcolum; i++) {

				array[i] = hr.getCell(i).getStringCellValue();
				System.out.println(array[i]);
				//field = cls.getDeclaredField(array[i]);
				//System.out.println(field);
				//System.out.println("=========" + field.getType() + "=====" + String.class);
			}

			for (int i = 1; i <= rowNum; i++) {
				T obj=(T)cls.newInstance();
				HSSFRow row = hs.getRow(i);
				for (int j = 0; j < lastcolum; j++) {
					
					if(array[j].contains(":")) {
						
						String[] fieldArray=array[j].split(":");
						
						String filedName=fieldArray[0];
						
						Field f=cls.getDeclaredField(filedName);
						f.setAccessible(true);
						
						int id1=(int)row.getCell(j).getNumericCellValue();
						String fieldClass=fieldArray[1];
						
						
						Class oclass=cls.forName(fieldClass);
						T objj=(T)oclass.newInstance();
						Field ff=oclass.getDeclaredField("id");
						
						ff.setAccessible(true);
						ff.set(objj, id1);
						
						Field fil=cls.getDeclaredField(filedName);
						fil.setAccessible(true);
						fil.set(obj, objj);
						
					}
					else {
					
					field = cls.getDeclaredField(array[j]);
					if (field.getType() == String.class) {
						//System.out.println("=========" + String.class);
						
						String numaric = row.getCell(j).getStringCellValue();
						field.setAccessible(true);
						field.set(obj, numaric);
						
						System.out.println("========" + numaric);

					} else if (field.getType() == Date.class) {

						String date = row.getCell(j).getStringCellValue();
						
						SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
						Date d = s.parse(date);
						field.setAccessible(true);
						field.set(obj, d);
						System.out.println("========" + date);
					} else if(field.getType() == Integer.class) {
						//System.out.println(Integer.class + "====" + field.getType());
						int numaric = (int) row.getCell(j).getNumericCellValue();
						System.out.println("================"+numaric);
						field.setAccessible(true);
						field.set(obj, numaric);
						System.out.println("====" + numaric);

					}
					else if(field.getType() == Long.class) {
						//System.out.println(Integer.class + "====" + field.getType());
						long numaric = (long) row.getCell(j).getNumericCellValue();
						field.setAccessible(true);
						field.set(obj, numaric);
						System.out.println("====" + numaric);

					}
					else if(field.getType() == Double.class) {
						//System.out.println(Integer.class + "====" + field.getType());
						double numaric = (double) row.getCell(j).getNumericCellValue();
						field.setAccessible(true);
						field.set(obj, numaric);
						System.out.println("====" + numaric);

					}

				}
				}
				obj1.add(obj);

			}
			fis.close();
			

		} catch (IOException e) {
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return obj1;
		}

}
