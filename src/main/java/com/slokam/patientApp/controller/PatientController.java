package com.slokam.patientApp.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.slokam.patientApp.exception.ApplicationException;
import com.slokam.patientApp.pojo.Appointment;
import com.slokam.patientApp.pojo.Patient;
import com.slokam.patientApp.pojo.Visite;
import com.slokam.patientApp.service.PatientService;
import com.slokam.patientApp.service.PatientServiceInterface;

@RestController
@RequestMapping("pat")
public class PatientController {
	
	@Autowired
	private RepeadtedCode repeadtedCode;
	
	@Autowired
	private PatientServiceInterface patientServiceInterface;
	
	private static Logger LOGGER=LoggerFactory.getLogger(PatientService.class);
	
	@PostMapping
	public ResponseEntity<String> patientSave(@RequestBody Visite p)throws ApplicationException {
		
		ResponseEntity<String> responseEntity=new ResponseEntity<>("Successfully inserted",HttpStatus.OK);
		
		
		LOGGER.debug("we are in contrller");
		
		//Patient p1=repeadtedCode.setNull(p);
		
		//Patient p1=repeadtedCode.setSave(p);
		patientServiceInterface.patientSave(p);
		
		LOGGER.debug("exit from contrller");
		
		return responseEntity;
		
		
	}
	@GetMapping
	public ResponseEntity<List<String>> getDoctors(Date date1,Date date2) throws ApplicationException {
		LOGGER.debug("we are in getDoctors");
		
		ResponseEntity<List<String>> res=null;
		
		try {
			
			List<String> ls=patientServiceInterface.getDoctors(date1, date2);
			 res=new ResponseEntity<List<String>>(ls,HttpStatus.OK);
			
		}catch(Exception e){
			e.printStackTrace();
			
		}
		
		LOGGER.debug("exit from  getDoctors");
		return res;
	}
	


}
