package com.slokam.patientApp.pojo;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="complaints")
public class Complaints {
	
	
	@Override
	public String toString() {
		return "Complaints [id=" + id + ", seveirity=" + seveirity + ", since=" + since + ", comments=" + comments
				+ ", visite=" + visite + ", disease=" + disease + "]";
	}
	@Id
	@GeneratedValue
	private int id;
	private String seveirity;
	private String since;
	private String comments;
	
	@ManyToOne
	@JoinColumn(name="vid")
	private Visite  visite;
	
	@ManyToOne
	@JoinColumn(name="did")
	private Disease disease;
	
	public Visite getVisite() {
		return visite;
	}
	public void setVisite(Visite visite) {
		this.visite = visite;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSeveirity() {
		return seveirity;
	}
	public void setSeveirity(String seveirity) {
		this.seveirity = seveirity;
	}
	public String getSince() {
		return since;
	}
	public void setSince(String since) {
		this.since = since;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}


	public Disease getDisease() {
		return disease;
	}
	public void setDisease(Disease disease) {
		this.disease = disease;
	}

}
