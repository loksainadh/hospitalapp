package com.slokam.patientApp.pojo;




import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import javax.persistence.Table;



@Entity
@Table(name="patient")
public class Patient  {
	
	

		
	
	@Id
	@GeneratedValue
	private Integer id;
	private String name;
	private Integer age;
	private Integer weight;
	private Date date;
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public Integer getWeight() {
		return weight;
	}
	public void setWeight(Integer weight) {
		this.weight = weight;
	}
	/*
	public Patient(Integer id, String name, Integer age, Integer weight, Date date) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.weight = weight;
		this.date = date;
	}*/
	@Override
	public String toString() {
		return "Patient [id=" + id + ", name=" + name + ", age=" + age + ", weight=" + weight + ", date=" + date + "]";
	}
	

	
	
}
