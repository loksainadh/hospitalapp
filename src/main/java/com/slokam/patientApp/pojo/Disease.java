package com.slokam.patientApp.pojo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Currency;
import org.hibernate.validator.constraints.Length;

@Entity
@Table(name="disease")
public class Disease {
	@Id
	@GeneratedValue
	private int id;
	
	@Length(min=2,max=15)
	@Pattern(regexp="^ram{2}$")
	private  String disease;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDisease() {
		return disease;
	}
	public void setDisease(String disease) {
		this.disease = disease;
	}

}
