package com.slokam.patientApp.pojo;


import java.util.List;

import javax.annotation.Generated;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="doctor")
public class Doctor {
	@Id
	@GeneratedValue
	private Integer id;
	private String name;
	private String typeOfDoctor;
	
	
	
	@Override
	public String toString() {
		return "Doctor [id=" + id + ", name=" + name + ", typeOfDoctor=" + typeOfDoctor + "]";
	}

	

	public Integer getId() {
		return id;
	}



	public void setId(Integer id) {
		this.id = id;
	}



	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTypeOfDoctor() {
		return typeOfDoctor;
	}

	public void setTypeOfDoctor(String typeOfDoctor) {
		this.typeOfDoctor = typeOfDoctor;
	}

	
}
