package com.slokam.patientApp.pojo;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="visit")
public class Visite {
	
	
	
	@Override
	public String toString() {
		return "Visite [id=" + id + ", appointment=" + appointment + ", doctor=" + doctor + ", visitDate=" + visitDate
				+ "]";
	}
	@Id
	@GeneratedValue
	private Integer id;
	
	
	@OneToOne
	@JoinColumn(name="aid")
	private Appointment appointment;
	
	@OneToOne
	@JoinColumn(name="did")
	private Doctor doctor;
	private Date visitDate;
	
	
	
	public Date getVisitDate() {
		return visitDate;
	}
	public void setVisitDate(Date visitDate) {
		this.visitDate = visitDate;
	}
	public Doctor getDoctor() {
		return doctor;
	}
	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Appointment getAppointment() {
		return appointment;
	}
	public void setAppointment(Appointment appointment) {
		this.appointment = appointment;
	}
	
	

}
