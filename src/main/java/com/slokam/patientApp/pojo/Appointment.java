package com.slokam.patientApp.pojo;

import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import javax.persistence.Table;

@Entity
@Table(name="appointment")
public class Appointment {
	@Id
	@GeneratedValue
	private Integer id;
	
	private  Date dateofApp;
	
	private Date reqdate;
	private String time;
	@ManyToOne
	@JoinColumn(name="pid")
	private Patient  patient;
	
	private String paid;
	private String free;
	
	
	
	public String getPaid() {
		return paid;
	}

	public void setPaid(String paid) {
		this.paid = paid;
	}

	public String getFree() {
		return free;
	}

	public void setFree(String free) {
		this.free = free;
	}

	

	
	

	@Override
	public String toString() {
		return "Appointment [id=" + id + ", dateofApp=" + dateofApp + ", reqdate=" + reqdate + ", time=" + time
				+ ", patient=" + patient + "]";
	}
	
	

	
	
	

	public Date getReqdate() {
		return reqdate;
	}

	public void setReqdate(Date reqdate) {
		this.reqdate = reqdate;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDateofApp() {
		return dateofApp;
	}

	public void setDateofApp(Date dateofApp) {
		this.dateofApp = dateofApp;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	
	
	

}
