package com.slokam.patientApp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.slokam.patientApp.pojo.Doctor;
import com.slokam.patientApp.pojo.Patient;
@Repository
public interface DoctorRepositry extends JpaRepository<Doctor, Integer> {
	@Query("select p from Visite v join v.appointment a join a.patient p join v.doctor d where d.name=?1")
	public abstract List<Patient> getPatients(String p);
	
	

}
