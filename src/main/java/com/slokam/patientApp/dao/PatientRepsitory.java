package com.slokam.patientApp.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.slokam.patientApp.pojo.Appointment;
import com.slokam.patientApp.pojo.Patient;
import com.slokam.patientApp.pojo.Visite;
@Repository
public interface PatientRepsitory extends JpaRepository<Visite, Integer> {
	@Query("select p.name from Visite v join v.appointment a join a.patient p join v.doctor d where  a.reqdate between ?1 and ?2   ")
	public abstract List<String> getDoctors(Date date1,Date date2);

}
