package com.slokam.patientApp.dao;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.slokam.patientApp.pojo.Appointment;

@Repository
public interface AppointmentInteface extends JpaRepository<Appointment, Integer> {

	@Query("select d.disease from Complaints c join c.visite v join  c.disease d join v.appointment a join v.doctor dt join a.patient p where a.reqdate between ?1 and ?2")
	public List<String> getDisease(LocalDateTime date1,LocalDateTime date2);
	
	@Query("select v.id,a.reqdate from Visite v join v.appointment a join v.doctor d join a.patient p where p.name=?1")
	public List<Object[]> getVisitType(String name);
	
	@Query("select a.reqdate from Visite v join v.appointment a join v.doctor d join a.patient p where v.id=?1")
	public List<Date> getVisit(int id);
	

}
