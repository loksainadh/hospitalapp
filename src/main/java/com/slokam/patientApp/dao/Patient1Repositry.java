package com.slokam.patientApp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.slokam.patientApp.pojo.Patient;

@Repository
public interface Patient1Repositry extends JpaRepository<Patient, Integer> {
	
	@Query("select d.name from Visite v join v.appointment a join a.patient p join v.doctor d where p.name=?1 ")
	public abstract List<String> getDoctorsName(String name);

}
