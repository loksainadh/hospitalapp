package com.slokam.patientApp.dao;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.slokam.patientApp.pojo.Complaints;

@Repository
public interface ComplaintsRepositry extends JpaRepository<Complaints, Integer>{
	

}
