package com.slokam.patientApp.dao;

import org.springframework.stereotype.Repository;

import com.slokam.patientApp.exception.ApplicationException;
import com.slokam.patientApp.pojo.Appointment;
import com.slokam.patientApp.pojo.Patient;
import com.slokam.patientApp.pojo.Visite;

@Repository
public interface PatientInterface {

	public abstract void patientSave(Visite p) throws ApplicationException;
	
	
}
