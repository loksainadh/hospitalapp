package com.slokam.patientApp.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.slokam.patientApp.exception.ApplicationException;
import com.slokam.patientApp.pojo.Appointment;
import com.slokam.patientApp.pojo.Patient;
import com.slokam.patientApp.pojo.Visite;
import com.slokam.patientApp.service.PatientService;

@Repository
public class PatientDao implements PatientInterface {
	
	private static Logger LOGGER=LoggerFactory.getLogger(PatientDao.class);
  @Autowired
	private PatientRepsitory  patientRepsitory;
	
	public void patientSave(Visite p) throws ApplicationException {
		LOGGER.debug("we are in Dao  method");
		patientRepsitory.save(p);
		LOGGER.debug("Exit from Dao  method");
		
	}

}
