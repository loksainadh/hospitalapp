package com.slokam.patientApp.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.slokam.patientApp.pojo.Disease;

@Repository
public interface DiseasRepositry extends JpaRepository<Disease, Integer>{
	
	@Query("select p.name from Complaints c join c.visite v join v.appointment a join a.patient p join c.disease d where d.disease=?1 ")
	public List<String> getPatients(String name);

	@Query("select count(p.name) from Complaints c join c.visite v join v.appointment a join a.patient p join c.disease d where d.disease=?1 ")
	public Integer getPatientCount(String name);

	@Query("select count(p.name) from Complaints c join c.visite v join v.appointment a join a.patient p join v.doctor d join c.disease di where di.disease=?1")
	public Integer getMostRecurrentDisease(String name);
	
	@Query("select a.reqdate  from Complaints c join c.visite v join v.appointment a join a.patient p join v.doctor d join c.disease di where di.disease=?1 ")
	public List<Date> getSeason1(String diseaseName);
	
	@Query("from Disease")
	public List<Disease> getDiseases();
	
	public  Disease findByDisease(String name);
	

	
}
