package com.slokam.patientApp.excelReadw;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

import com.slokam.patientApp.exception.ApplicationException;
import com.slokam.patientApp.pojo.Appointment;
import com.slokam.patientApp.pojo.Doctor;
import com.slokam.patientApp.pojo.Patient;
import com.slokam.patientApp.service.PateintServiceP;
import com.slokam.patientApp.service.PatientInterfaceService;

@Service
public  class ExcelRead {
	
private static	Logger LOGGER=LoggerFactory.getLogger(ExcelRead.class);
@Autowired
private PatientInterfaceService patientInterfaceService;
@Autowired
public static String filepath;


	public   List<Patient> readExcel()throws ApplicationException{
	
		LOGGER.debug("enter excel read   =================="+filepath);
		List<Patient> lp=new ArrayList<>();
		
		try {
		FileInputStream fir=new FileInputStream(filepath);
		LOGGER.debug("try block   ==================");
		
		HSSFWorkbook hw=new HSSFWorkbook(fir);
		HSSFSheet hs=hw.getSheetAt(0);
		HSSFSheet hs1=hw.getSheet("Doctor.java");
		
		int num=hs.getLastRowNum();
		
		for(int i=0;i<num;i++) {
			LOGGER.debug("enter excel read   ==================loop");
			HSSFRow hr=hs.getRow(i);
			Patient p=new Patient();
			p.setName(hr.getCell(1).getStringCellValue());
			int age=(int)hr.getCell(2).getNumericCellValue();
			p.setAge(age);
			
			LOGGER.debug("enter excel read   =================="+p.getName());
			p.setWeight((int)hr.getCell(3).getNumericCellValue());
			String date=hr.getCell(4).getStringCellValue();
			
			try {
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
			
			Date date1=sdf.parse(date);
			
			p.setDate(date1);
			
			}
			catch(Exception e) {
				e.printStackTrace();
				
			}
			
			LOGGER.debug("excell read========="+p.getDate());
			
			lp.add(p);
			
			
			
			
			
		}
		
		
		
		
		fir.close();
		}
		catch(IOException e) {
			e.printStackTrace();
			throw new ApplicationException("Db problem",e);
			
		}
		
		return lp;
		
	}
	
	
	public   List<Appointment> readExcelForAppoit()throws ApplicationException{
		
		
		LOGGER.debug("enter excel read for doctor  ==================");
		
		List<Appointment> ld=new ArrayList<>();
		try {
		FileInputStream fir=new FileInputStream(filepath);
		
		HSSFWorkbook hw=new HSSFWorkbook(fir);
		HSSFSheet hs=hw.getSheet("Appointment.java");
	int rowNum=	hs.getLastRowNum();
	for(int i=0;i<=rowNum;i++) {
		HSSFRow hr=hs.getRow(i);
		Appointment d=new Appointment();
		String date=hr.getCell(1).getStringCellValue();
		try {
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		Date dd=sdf.parse(date);
		d.setDateofApp(dd);
		}
		catch(Exception e) {
			e.printStackTrace();
			
		}
	String date2=	hr.getCell(2).getStringCellValue();
	
	try {
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		Date dd=sdf.parse(date);
		d.setReqdate(dd);
		}
		catch(Exception e) {
			e.printStackTrace();
			
		}
	String time=hr.getCell(3).getStringCellValue();
	d.setTime(time);
	List<Patient>  lp=patientInterfaceService.pateintSave1();
	Map<Integer, Integer> patientMap=new LinkedHashMap<>();
	int j=1;
	for(Patient p:lp) {
		patientMap.put(j, p.getId());
		j++;
	}
	//int id=0;
	//List<Integer> li=new ArrayList<>();
	//for(int k=1;k<patientMap.size();k++) {
	// id=patientMap.get(k);
	// li.add(id);
	//}
	Patient pa=new Patient();
	//for(int k=i;k<=i;k++) {
		//pa.setId(li.get(k));
		//}
int id=(int)hr.getCell(4).getNumericCellValue();
int pid=patientMap.get(id);
pa.setId(pid);
	
	d.setPatient(pa);
	ld.add(d);
	
	}
	
	fir.close();}
		catch(IOException e) {
			e.printStackTrace();
		}
		LOGGER.debug("Exit  excel read for doctor  ==================");
			
		return ld;
		}
	
	
	public static  List<Doctor> readExcelForDoctor()throws ApplicationException{
		
	
		LOGGER.debug("enter excel read for doctor  ==================");
		
		List<Doctor> ld=new ArrayList<>();
		try {
		FileInputStream fir=new FileInputStream(filepath);
		
		HSSFWorkbook hw=new HSSFWorkbook(fir);
		HSSFSheet hs=hw.getSheet("Doctor.java");
	int rowNum=	hs.getLastRowNum();
	for(int i=0;i<=rowNum;i++) {
		HSSFRow hr=hs.getRow(i);
		Doctor d=new Doctor();
		String name=hr.getCell(1).getStringCellValue();
		String speci=hr.getCell(2).getStringCellValue();
		d.setName(name);
		d.setTypeOfDoctor(speci);
		ld.add(d);
		
	}}
		catch(IOException e) {
			e.printStackTrace();
		}
		LOGGER.debug("Exit  excel read for doctor  ==================");

			
		return ld;
		}
		
		
		
		
		
				
		
		
		
		
	

}
